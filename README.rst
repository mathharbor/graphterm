Use the following command to run GraphTerm:
python graphterm/gtermserver.py --host=0.0.0.0 --port=8000 --auth_type=none --allow_embed

If this is to run inside a Docker container, use the following command:
/usr/bin/python /home/graphterm/graphterm/gtermserver.py --host=0.0.0.0 --port=8000 --auth_type=none --allow_embed
